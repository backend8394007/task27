const express = require('express');
const userRouter = require('./routes/user.routes');

const app = express();

app.use(express.json());
app.use('/api', userRouter);

const port = process.env.PORT || 3000;

app.listen(port, (err) => {
   err ? console.error(err) : console.log(`The server is listening on port: ${port}`)
});