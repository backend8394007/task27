const db = require('../db');

class UserController{
    async createUser(req, res){
        const { name, age } = req.body;
        const queryText = 'INSERT INTO person (name, age) VALUES ($1, $2) RETURNING *';
        const values = [name, age];
        try {
            const newUser = await db.query(queryText, values);
            res.status(201).json(newUser.rows[0]);
        } catch (error) {
            res.status(500).json({error: error.message});
        }
    }
    async getUsers(req, res){
        try {
            const users = await db.query('SELECT * FROM person');
            res.status(200).json(users.rows);
        } catch (error){
            res.status(500).json({error: error.message});
        }
    }
    async getOneUser(req, res){
        const userId = req.params.id;
        try {
            const user = await db.query('SELECT * FROM person WHERE id = $1', [userId]);
            if (user.rows.length === 0){
                res.status(404).json({message: 'User not found'});
            }else{
                res.status(200).json(user.rows[0]);
            }
        } catch (error){
            res.status(500).json({error: error.message});
        }
    }
    async updateUser(req, res){
        const userId = req.params.id;
        const {name, age} = req.body;
        try {
            const updatedUser = await db.query('UPDATE person SET name = $1, age = $2 WHERE id = $3 RETURNING *', [name, age, userId]);
            if (updatedUser.rows.length === 0){
                res.status(404).json({message: 'User not found'});
            }
            else{
                res.status(200).json(updatedUser.rows[0]);
            }
        } catch (error){
            res.status(500).json({error: error.message});
        }
    }
    async deleteUser(req, res){
        const userId = req.params.id;
        try {
            const deletedUser = await db.query('DELETE FROM person WHERE id = $1 RETURNING *', [userId]);
            if (deletedUser.rows.length === 0){
                res.status(404).json({message: 'User not found'});
            }
            else{
                res.status(200).json({ message: 'User deleted successfully' });
            }
        } catch (error){
            res.status(500).json({error: error.message});
        }
    }
}

module.exports = new UserController();